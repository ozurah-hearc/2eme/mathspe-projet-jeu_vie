/*-----------------------------------------
  Mathématiques Spécifiques II (Algo. Num.)
  Projet - Automate cellulaire paramétrable
  09.06.2022

  Equipe 03 : M. Allemann Jonas
              M. Chappuis Sebastien
              Mme. Michel Jeanne
              M. Stouder Xavier

  Notes :
    - L'opération "nombre | 0" permet de tronquer un flottant vers un entier en retirant sa partie décimale
    - L'opérateur "+nombre" effectue un cast implicit de string à flottant
------------------------------------------*/

;(function () {
    'use strict'

    // +--------------------+
    // |  Global constants  |
    // +--------------------+

    const CELLS_COLOR = '#EEE'
    const CURSOR_COLOR = 'rgba(64,128,255,0.5)'

    // +--------------------+
    // |   HTML elements    |
    // +--------------------+

    const inputSpeed = document.getElementById('input-speed')
    const inputOob = document.getElementById('input-oob')
    const inputCellsSize = document.getElementById('input-cells-size')
    const inputAdapt = document.getElementById('input-adapt')
    const inputAdaptFrequency = document.getElementById('input-adapt-frequency')
    const inputRuleset = document.getElementById('input-ruleset')
    const inputsCheckbox = []
    const inputRandomizeRuleset = document.getElementById(
        'input-randomize-ruleset'
    )
    const inputRandomizeCells = document.getElementById('input-randomize-cells')

    const rulesetWrapper = document.getElementById('ruleset-wrapper')

    const wrapper = document.getElementById('game-wrapper')
    const canvas = document.getElementById('game')
    const ctx = canvas.getContext('2d')

    // +--------------------+
    // |  Global variables  |
    // +--------------------+

    let nbIterPerSec
    let areCellsLooped

    let interval = null

    let grid
    let gridW
    let gridH
    let cellsSize

    let isLeftClickPressed = false
    let isRightClickPressed = false
    let hoveredCellX = null
    let hoveredCellY = null

    let rulesetLookupTable = new Array(2 ** 9).fill(false)

    let chart = null
    let iterationCounter = 0
    let isRulesetAdapdative = false
    let densityHistory = []
    let stabilityHistory = []
    let nbFramesBetweenAdapts = 0

    // +--------------------+
    // |       Events       |
    // +--------------------+

    inputSpeed.addEventListener('input', update)
    inputOob.addEventListener('input', update)
    inputCellsSize.addEventListener('input', update)
    inputAdapt.addEventListener('input', update)
    inputAdaptFrequency.addEventListener('input', update)
    inputRuleset.addEventListener('input', updateRuleset)
    inputRandomizeRuleset.addEventListener('click', randomizeRuleset)
    inputRandomizeCells.addEventListener('click', randomizeCells)

    window.addEventListener('resize', resize)
    window.addEventListener('mousedown', mouseClick, true)
    window.addEventListener('mousemove', mouseClick)
    window.addEventListener('mouseup', mouseClick)

    canvas.addEventListener('contextmenu', (ev) => ev.preventDefault())

    // +--------------------+
    // |     Functions      |
    // +--------------------+

    /**
     * Init the UI elements (ruleset + chart)
     * Set the first ruleset
     * Start the automaton
     */
    function init() {
        let i = 0

        // Display and add events to the checkboxes for the ruleset
        // rows
        for (let y = 0; y < 16; y++) {
            const row = document.createElement('div')
            row.classList.add('rule-row')

            // columns
            for (let x = 0; x < 32; x++) {
                const checkbox = document.createElement('div')

                checkbox.classList.add('rule')

                checkbox.dataset.index = i
                checkbox.dataset.selected = CONWAY_RULESET[i]

                checkbox.addEventListener('click', (ev) => ev.preventDefault())
                checkbox.addEventListener('contextmenu', (ev) =>
                    ev.preventDefault()
                )

                checkbox.addEventListener('mousedown', updateRule)
                checkbox.addEventListener('mouseenter', updateRule)

                for (let j = 0; j < 9; j++) {
                    const elt = document.createElement('div')
                    elt.classList.add('visual-element')
                    if ((i >> j) & 0b1) elt.classList.add('on')
                    checkbox.appendChild(elt)
                }

                inputsCheckbox.push(checkbox)
                row.appendChild(checkbox)

                i++
            }

            rulesetWrapper.appendChild(row)
        }

        rulesetLookupTable = CONWAY_RULESET.concat() // Clone the array

        // Set the chart UI
        chart = new window.CanvasJS.Chart('chart', {
            title: {
                text: 'Progression',
            },
            theme: 'dark2',
            axisY: {
                title: 'Percentage',
                minimum: 0,
                maximum: 100,
            },
            legend: {
                verticalAlign: 'top',
                horizontalAlign: 'center',
            },
            data: [
                {
                    name: 'Density',
                    showInLegend: true,
                    type: 'line',
                    color: '#FF8000',
                    dataPoints: densityHistory,
                },
                {
                    name: 'Stability',
                    showInLegend: true,
                    type: 'line',
                    color: '#0080FF',
                    dataPoints: stabilityHistory,
                },
            ],
        })

        chart.render()

        // Set default ruleset and start the automaton execution
        updateRuleset()
        update()
    }

    /**
     * Apply a random ruleset + update the checkboxes
     */
    function randomizeRuleset() {
        inputRuleset.value = 'custom'

        for (let i = 0; i < rulesetLookupTable.length; i++) {
            const state = Math.random() < 0.5

            rulesetLookupTable[i] = state
            inputsCheckbox[i].dataset.selected = state
        }
    }

    /**
     * Apply a random cells on the automaton grid
     */
    function randomizeCells() {
        grid = new Array(gridW * gridH)
            .fill(false)
            .map(() => Math.random() < 0.2)

        redraw()
    }

    /**
     * Event on the checkboxes for the ruleset, used to update the state of the custom rule when left or right mouse pressed
     * @param {*} ev Event who call this method
     */
    function updateRule(ev) {
        const checkbox = ev.currentTarget
        const index = +checkbox.dataset.index

        if (isLeftClickPressed ^ isRightClickPressed) {
            inputRuleset.value = 'custom'

            rulesetLookupTable[index] = isLeftClickPressed
            checkbox.dataset.selected = isLeftClickPressed
        }
    }

    /**
     * Update the ruleset with a presset.
     */
    function updateRuleset() {
        switch (inputRuleset.value) {
            case 'conway':
                rulesetLookupTable = CONWAY_RULESET.concat() // Clone the array
                break

            case 'empty':
                rulesetLookupTable.fill(false)
                break

            case 'full':
                rulesetLookupTable.fill(true)
                break

            case 'stable':
                rulesetLookupTable = rulesetLookupTable.map((rule, i) => {
                    return i % 32 >= 16
                })
                break
        }

        // Update the checkboxes from the ruleset
        inputsCheckbox.forEach((checkbox, i) => {
            checkbox.dataset.selected = rulesetLookupTable[i]
        })
    }

    /**
     * Update the automaton grid + start the compute for next generation at periodic time (according the speed)
     * (Compute means get the next generation and draw it)
     *
     * If the automaton isn't in adaptive mode, the ruleset stability frequency is disabled.
     */
    function update() {
        const newNbIterPerSec = +inputSpeed.value

        if (newNbIterPerSec !== nbIterPerSec) {
            nbIterPerSec = newNbIterPerSec

            clearInterval(interval)

            if (nbIterPerSec > 0) {
                interval = setInterval(nextIteration, 1000 / nbIterPerSec)
            }
        }

        nbIterPerSec = +inputSpeed.value
        areCellsLooped = inputOob.value === 'loop-cells'

        const newCellsSize = +inputCellsSize.value

        if (newCellsSize !== cellsSize) {
            cellsSize = newCellsSize
            resize()
        }

        isRulesetAdapdative = inputAdapt.value === 'enabled'
        nbFramesBetweenAdapts = 100 - inputAdaptFrequency.value

        inputAdaptFrequency.disabled = !isRulesetAdapdative
    }

    /**
     * Update the canvas size + randomize the cells (like it's a new automaton, the grid have changed)
     */
    function resize() {
        const rect = wrapper.getBoundingClientRect()

        gridW = (rect.width / cellsSize) | 0
        gridH = (rect.height / cellsSize) | 0

        canvas.width = gridW * cellsSize
        canvas.height = gridH * cellsSize

        randomizeCells()
    }

    /**
     * Perform the calculation of the next generation of the automaton + draw it
     */
    function nextIteration() {
        const newGrid = new Array(gridW * gridH)

        for (let y = 0; y < gridH; y++) {
            for (let x = 0; x < gridW; x++) {
                // Get the index of the cell in the ruleset
                let index = 0
                index |= 1 * getCellAt(grid, x - 1, y - 1)
                index |= 2 * getCellAt(grid, x + 0, y - 1)
                index |= 4 * getCellAt(grid, x + 1, y - 1)
                index |= 8 * getCellAt(grid, x - 1, y + 0)
                index |= 16 * getCellAt(grid, x + 0, y + 0)
                index |= 32 * getCellAt(grid, x + 1, y + 0)
                index |= 64 * getCellAt(grid, x - 1, y + 1)
                index |= 128 * getCellAt(grid, x + 0, y + 1)
                index |= 256 * getCellAt(grid, x + 1, y + 1)

                const state = rulesetLookupTable[index]
                // set the next generation state of this cell
                setCellAt(newGrid, x, y, state)
            }
        }

        const density = computeDensity()
        const stability = computeStability(grid, newGrid)

        // Update the ruleset when required
        if (
            isRulesetAdapdative &&
            iterationCounter % nbFramesBetweenAdapts === 0
        ) {
            adaptRuleset()
        }

        // Update the chart
        densityHistory.push({
            x: iterationCounter,
            y: 100 * density,
        })

        stabilityHistory.push({
            x: iterationCounter,
            y: 100 * stability,
        })

        // Only keep the data from the last 100 iterations
        if (densityHistory.length > 100) {
            densityHistory.shift()
            stabilityHistory.shift()
        }

        const densityProgression = computesDensityProgression()

        chart.render()

        grid = newGrid

        iterationCounter++

        redraw()
    }

    /**
     *
     * @returns Get the density of alive cells
     */
    function computeDensity() {
        const sum = grid.reduce((a, b) => +a + +b, 0)

        return sum / grid.length
    }

    /**
     * Compute the stability of the automaton on 2 generations
     * @param {*} gridA The previous state
     * @param {*} gridB The new state
     * @returns Get the stability of the automaton
     */
    function computeStability(gridA, gridB) {
        let sumDeltaStates = 0

        for (let i = 0; i < grid.length; i++) {
            const cellA = gridA[i]
            const cellB = gridB[i]
            if (cellA ^ cellB) {
                sumDeltaStates++
            }
        }

        return 1 - sumDeltaStates / grid.length
    }

    /**
     * Compute the averaged density over the last 100 iterations
     * (use the difference between the average of 100th to 51th and the average if 50th to 0th) to have better results
     * @returns Get the density progression
     */
    function computesDensityProgression() {
        if (densityHistory.length < 2) return 0

        const deltaX = densityHistory.length / 2

        const firstHalf = densityHistory.map((val) => val.y)
        const secondHalf = firstHalf.splice(deltaX | 0)

        const firstAvg =
            firstHalf.reduce((a, b) => a + b, 0) / firstHalf.length / 100
        const secondAvg =
            secondHalf.reduce((a, b) => a + b, 0) / secondHalf.length / 100

        const deltaY = secondAvg - firstAvg

        return deltaY / deltaX
    }

    /**
     * Algorithm to adapt the ruleset according to the stability progression
     * @returns
     */
    function adaptRuleset() {
        if (densityHistory.length === 0 || stabilityHistory.length === 0) {
            return
        }

        inputRuleset.value = 'custom'

        const avgDensity =
            densityHistory.map((e) => e.y / 100).reduce((a, b) => a + b) /
            densityHistory.length

        const avgStability =
            stabilityHistory.map((e) => e.y / 100).reduce((a, b) => a + b) /
            stabilityHistory.length

        // Too low density to adapte, add a random value (this will increase the alive cells)
        if (avgDensity < 0.02) {
            const rulesetIndexesOff = []

            rulesetLookupTable.forEach((rule, i) => {
                if (!rule) rulesetIndexesOff.push(i)
            })

            const index = getRandomValue(rulesetIndexesOff)
            rulesetLookupTable[index] = true
            inputsCheckbox[index].dataset.selected = true

            return
        }

        // Too high stability to adapte, remove a random rule (this will decrease the alive cells)
        if (avgDensity > 0.98) {
            const rulesetIndexesOn = []

            rulesetLookupTable.forEach((rule, i) => {
                if (rule) rulesetIndexesOn.push(i)
            })

            const index = getRandomValue(rulesetIndexesOn)
            rulesetLookupTable[index] = false
            inputsCheckbox[index].dataset.selected = false

            return
        }

        // Analize 1% of all cells
        const nbCellsToAnalyse = (grid.length / 100) | 0

        for (let i = 0; i < nbCellsToAnalyse; i++) {
            const x = (Math.random() * gridW) | 0
            const y = (Math.random() * gridH) | 0

            const state = getCellAt(grid, x, y)

            let ruleIndex = 0

            ruleIndex |= 1 * getCellAt(grid, x - 1, y - 1)
            ruleIndex |= 2 * getCellAt(grid, x + 0, y - 1)
            ruleIndex |= 4 * getCellAt(grid, x + 1, y - 1)
            ruleIndex |= 8 * getCellAt(grid, x - 1, y + 0)
            ruleIndex |= 16 * state
            ruleIndex |= 32 * getCellAt(grid, x + 1, y + 0)
            ruleIndex |= 64 * getCellAt(grid, x - 1, y + 1)
            ruleIndex |= 128 * getCellAt(grid, x + 0, y + 1)
            ruleIndex |= 256 * getCellAt(grid, x + 1, y + 1)

            // Update the rule according the state of the analyzed cell
            rulesetLookupTable[ruleIndex] = state
            inputsCheckbox[ruleIndex].dataset.selected = state
        }
    }

    /**
     * Clean the canvas and draw the current generation on it
     */
    function redraw() {
        ctx.clearRect(0, 0, canvas.width, canvas.height)
        ctx.fillStyle = CELLS_COLOR

        for (let y = 0; y < gridH; y++) {
            for (let x = 0; x < gridW; x++) {
                const state = getCellAt(grid, x, y)

                if (state) {
                    const margin = 1

                    ctx.fillRect(
                        cellsSize * x + margin,
                        cellsSize * y + margin,
                        cellsSize - 2 * margin,
                        cellsSize - 2 * margin
                    )
                }
            }
        }

        ctx.fillStyle = CURSOR_COLOR
        ctx.fillRect(
            cellsSize * hoveredCellX,
            cellsSize * hoveredCellY,
            cellsSize,
            cellsSize
        )
    }

    /**
     * Event when mouse is pressed
     * (to detect when mouse pressed and moving over automaton cells and ruleset)
     * @param {*} ev
     */
    function mouseClick(ev) {
        const rect = canvas.getBoundingClientRect()

        const mouseX = ev.pageX - scrollX - rect.x
        const mouseY = ev.pageY - scrollY - rect.y

        hoveredCellX = (mouseX / cellsSize) | 0
        hoveredCellY = (mouseY / cellsSize) | 0

        // Which button pressed/released ?
        if (ev.type === 'mousedown') {
            if (ev.which === 1) isLeftClickPressed = true
            if (ev.which === 3) isRightClickPressed = true
        } else if (ev.type === 'mouseup') {
            if (ev.which === 1) isLeftClickPressed = false
            if (ev.which === 3) isRightClickPressed = false
        }

        // Mouse move over the automaton cells ?
        if (isLeftClickPressed ^ isRightClickPressed) {
            const state = isLeftClickPressed

            if (
                hoveredCellX >= 0 &&
                hoveredCellX < gridW &&
                hoveredCellY >= 0 &&
                hoveredCellY < gridH
            ) {
                setCellAt(grid, hoveredCellX, hoveredCellY, state)
            }
        }

        redraw()
    }

    /**
     * Return the corresponding index or -1 if out of bounds
     * @param {*} grid 1D Array of bool, the cells of the automaton
     * @param {*} x Coordinate X of the cell
     * @param {*} y Coordinate Y of the cell
     * @returns
     */
    function getCellIndexAt(grid, x, y) {
        if (areCellsLooped) {
            // Wrap positions from range [0, size-1]

            x = (x + gridW) % gridW
            y = (y + gridH) % gridH
        } else {
            // Check that the positions is not out of bounds

            if (x < 0 || x >= gridW) return -1
            if (y < 0 || y >= gridH) return -1
        }

        return y * gridW + x
    }

    /**
     * Get the state of the cell at the given position
     * @param {*} grid 1D Array of bool, the cells of the automaton
     * @param {*} x Coordinate X of the cell
     * @param {*} y Coordinate Y of the cell
     * @returns
     */
    function getCellAt(grid, x, y) {
        const index = getCellIndexAt(grid, x, y)

        if (index > -1) return grid[index]
        return false
    }

    /**
     * Set the alive state of the cell at the given position
     * @param {*} grid 1D Array of bool, the cells of the automaton
     * @param {*} x Coordinate X of the cell
     * @param {*} y Coordinate Y of the cell
     * @param {*} state is the cell alive ?
     */
    function setCellAt(grid, x, y, state) {
        const index = getCellIndexAt(grid, x, y)

        if (index > -1) grid[index] = state
    }

    /**
     * Get a random value from the given array
     * @param {*} array
     * @returns
     */
    function getRandomValue(array) {
        return array[(Math.random() * array.length) | 0]
    }

    // +---------------------+
    // | START THE AUTOMATON |
    // +---------------------+

    init()
})()
